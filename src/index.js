const express = require('express');
const cors = require('cors');
const routes = require('./routes');

require('./database');

const server = express();

server.use(express.json());
server.use(cors());

server.use(routes);

server.listen(8000);


// docker run --name meu-db -p 5432:5432 -e POSTGRES_PASSWORD=docker -d 
// -v "/home/klinton/Desktop/volume-exemplo:/var/lib/postgresql/data" postgres:11

// --name: nome para o container
// -p: 5432:5432
// -d: background
// -v: "minhamáquina:container"
// postgres:11 imagem