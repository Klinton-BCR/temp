module.exports = {
  dialect: 'postgres',
  host: 'localhost',
  port: '54321',
  username: 'postgres',
  password: 'docker',
  database: 'temp',
  define: {
    timestamps: true,
  }
};