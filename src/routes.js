const { Router } = require('express');
const jwt = require('jsonwebtoken');

const User = require('./app/models/User');
const AuthMiddleware = require('./app/middleware/auth');

const routes = Router();

routes.get('/sessions', async (req, res) => {
  const user = await User.findOne({ where: { id: 1 } });

  if(!user) {
    return res.status(400).json({ mesage: 'Crie um usuário' });
  }
  const token = jwt.sign({ id: user.id }, 'secret', {});

  return res.json(token);
})

routes.get('/users', AuthMiddleware, async (req, res) => {
  const { id } = req.decoded;

  const user = await User.findOne({ where: { id } });

  return res.json(user);
});

routes.post('/users', async(req, res) => {
  const user = await User.create(req.body);
  // {
  //   "name": "Teste",
  //   "email": "email@email.com",
  //   "operacao": "operacao"
  // }

  return res.json(user);
});

module.exports = routes;