module.exports = {
  up: (queryInterface, Sequelize) => {
     return queryInterface.createTable('Users', {
        id: {
           type: Sequelize.INTEGER,
           allowNull: false,
           autoIncrement: true,
           primaryKey: true
        },
        name: {
           type: Sequelize.STRING,
           allowNull: false
        },
        email: {
           type: Sequelize.STRING,
           allowNull: false
        },
        operacao: {
           type: Sequelize.STRING,
           allowNull: false
        },
        createdAt: {
           type: Sequelize.DATEONLY,
           allowNull: false
        },
        updatedAt: {
           type: Sequelize.DATEONLY,
           allowNull: false
        }
     })
  },

  down: (queryInterface, Sequelize) => {
     return queryInterface.dropTable('Users')
  }
}